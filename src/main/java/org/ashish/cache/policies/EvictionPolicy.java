package org.ashish.cache.policies;

public interface EvictionPolicy <Key>{
    Key getEvictedKey();

    void keyAccessed(Key key);
}
