package org.ashish.cache;

import org.ashish.cache.policies.EvictionPolicy;
import org.ashish.cache.storage.Storage;

public class Cache <Key,Value>{
    private EvictionPolicy<Key> evictionPolicy;
    private Storage<Key,Value> storage;

    public Cache(EvictionPolicy<Key> evictionPolicy, Storage<Key, Value> storage) {
        this.evictionPolicy = evictionPolicy;
        this.storage = storage;
    }

    public void put(Key key, Value value){

    }

    public Value get(Key key){
        return null;
    }

    public void remove(Key key){

    }

    public void clearCache(){

    }
}
